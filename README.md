# Legend Keeper Integration

This module is not affiliated with with _LegendKeeper_, and is an unofficial integration to import _LegendKeeper_ exports into FoundryVTT as journal entries.

_LegendKeeper_ is a copyright of Algorific LLC.

![Screenshot of Legend Keeper sidebar](https://mattsmithin.nyc3.cdn.digitaloceanspaces.com/assets/lk-overview.png)

## Installation

- Install in the module manager via the following manifest: https://gitlab.com/asacolips-projects/foundry-mods/legendkeeper-integration/-/raw/master/module.json
- Enable the module in your world.

## Usage

> **BACKUP YOUR WORLD**
> Before attempting to use this module, backup your world in Foundry to a safe location.

### 1. Export your LegendKeeper wiki

1. Log in to your LegendKeeper wiki
2. Click your profile picture in the bottom left corner.
3. Go to **Project** > **Settings**
4. Click the **Export wiki** button
5. Change the **Export Type** to **JSON**
6. Keep an eye on your email and download the resulting zip file with your wiki's JSON data.

![Screenshot of exporting a LegendKeeper wiki](https://mattsmithin.nyc3.cdn.digitaloceanspaces.com/assets/lk-export.png)

### 2. Prepare your LK export directory

After downloading your LegendKeeper JSON export zip, extract it. Place the resulting folder somewhere that Foundry can access it, such as `<User Data>/worlds/myworld/legendkeeper`.

### 3. Let the module know where your directory was placed

In Foundry's right sidebar, Click the gear icon to change to the settings tab and then go to **Configure Settings**. In the settings window, go to the **Module Settings** tab. Find the **Legend Keeper Integration** settings group and choose the path that correlates with where you placed your wiki export in step 2 earlier. You can use the file button to the right of the path text field to choose the directory using Foundry's file browser.

![Screenshot of LegendKeeper module settings](https://mattsmithin.nyc3.cdn.digitaloceanspaces.com/assets/lk-settings.png)

### 4. Import your LegendKeeper wiki

Reload your world and go to the **Journal** tab of Foundry's right sidebar. Use the **Import from Legend Keeper** button at the bottom of the right sidebar to import your LegendKeeper journal entries.

### 5. Do not update LegendKeeper entries in Foundry itself

Journal entries created by LegendKeeper have a special flag placed on them that will allow them to be replaced whenever you perform a new import of your exported wiki. LegendKeeper journal entries have a special **LK** icon to the left of their name or folder name so that you know which ones were created by LegendKeeper. You can make changes to these journal entries, but any changes made will be lost as soon as you do a new import.

## A note on hierarchy

LegendKeeper supports any amount of folder depth you want, but Foundry tops out at 3 levels of nesting. As a result of this, folders deeper than 3 levels deep will be created as a level 3 directory and will be prefixed with hyphens to show their effective depth.

LegendKeeper also supports treating articles as folders, which Foundry does not do. To account for this, every folder created by LegendKeeper will also have a `[ROOT ARTICLE]---------------` entry that houses its text.